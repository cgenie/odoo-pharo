# Odoo RPC and simple UI written in Pharo Smalltalk

To start with, load the library in Iceberg, then run in Playground:

```smalltalk
| o omb op |
o := OdooRPC new.
o server: '<odoo-server>'.
o db: '<odoo-db>'.
o login: '<email>' password: '<password>'.

(OdooUIModelExplorer on: o)
   openWithSpec

```

Enter the model name, for example `product.template`, `mail.mail` or `sale.order` and browse away.
