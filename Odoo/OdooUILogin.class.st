"
Display a login screen for Odoo.
"
Class {
	#name : #OdooUILogin,
	#superclass : #ComposablePresenter,
	#instVars : [
		'acceptBlock',
		'db',
		'dbLabel',
		'host',
		'hostLabel',
		'login',
		'loginLabel',
		'password',
		'passwordLabel',
		'toolbar'
	],
	#category : #Odoo
}

{ #category : #specs }
OdooUILogin class >> defaultSpec [
  | rowWidth |

  rowWidth := 55.

  ^ SpecLayout composed
    newColumn: [ : c |
      c newRow: [ : r | r add: #hostLabel width: rowWidth.
	                     r add: #host ].
      c newRow: [ : r | r add: #dbLabel width: rowWidth.
	                     r add: #db ].
      c newRow: [ : r | r add: #loginLabel width: rowWidth.
	                     r add: #login ].
      c newRow: [ : r | r add: #passwordLabel width: rowWidth.
	                     r add: #password ].
      c newRow: [ :r | ].
      c newRow: [ :r | r add: #toolbar width: rowWidth ]
    ].


]

{ #category : #initialization }
OdooUILogin >> db [
  ^ db
]

{ #category : #initialization }
OdooUILogin >> dbLabel [
  ^ dbLabel
]

{ #category : #initialization }
OdooUILogin >> host [
  ^ host
]

{ #category : #initialization }
OdooUILogin >> hostLabel [
  ^ hostLabel
]

{ #category : #initialization }
OdooUILogin >> initializePresenter [
	toolbar okAction: [
			acceptBlock ifNotNil: [ :ab | ab value: { host text . db text . login text . password text } ].
			self window close
	].
]

{ #category : #initialization }
OdooUILogin >> initializeWidgets [
	
	host := self newTextInput.
	host autoAccept: true.
	hostLabel := self newLabel.
	hostLabel label: 'Host:'.
	
	db := self newTextInput.
	db autoAccept: true.
	dbLabel := self newLabel.
	dbLabel label: 'DB:'.
	
	login := self newTextInput.
	login autoAccept: true.
	loginLabel := self newLabel.
	loginLabel label: 'Login:'.
	
	password := self newTextInput.
	password encrypted: true.
	password autoAccept: true.
	passwordLabel := self newLabel.
	passwordLabel label: 'Password:'.
	
	toolbar := self instantiate: OkToolbar.

   self focusOrder
		add: host;
		add: db;
		add: login;
		add: password

]

{ #category : #initialization }
OdooUILogin >> login [
  ^ login
]

{ #category : #initialization }
OdooUILogin >> loginLabel [
  ^ loginLabel
]

{ #category : #accessing }
OdooUILogin >> onAccept: a [
  acceptBlock := a
]

{ #category : #initialization }
OdooUILogin >> password [
  ^ password
]

{ #category : #initialization }
OdooUILogin >> passwordLabel [ 
  ^ passwordLabel
]

{ #category : #api }
OdooUILogin >> title [
	^ 'Login to Odoo'
]

{ #category : #initialization }
OdooUILogin >> toolbar [
  ^ toolbar
]
