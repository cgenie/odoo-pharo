"
Form view for an Odoo entity (a Dictionary).
"
Class {
	#name : #OdooUIModelFormView,
	#superclass : #ComposablePresenter,
	#instVars : [
		'odooRPCModelBrowser',
		'entity',
		'dynamicModelView'
	],
	#category : #Odoo
}

{ #category : #specs }
OdooUIModelFormView class >> defaultSpec [
	^ SpecLayout composed
		newColumn: [ : col |
			col newRow: [ :row |
				row add: #dynamicModelView
			]
		]
]

{ #category : #'instance creation' }
OdooUIModelFormView class >> on: anEntity odooRPCModelBrowser: anOdooRPCModelBrowser [
	| inst |
	inst := self basicNew.
	inst entity: anEntity.
	inst odooRPCModelBrowser: anOdooRPCModelBrowser.
	inst initialize.
	^inst
]

{ #category : #access }
OdooUIModelFormView >> dynamicModelView [
	^ dynamicModelView
]

{ #category : #accessing }
OdooUIModelFormView >> entity: aDictionary [
	entity := aDictionary.
	
]

{ #category : #initialization }
OdooUIModelFormView >> initializeWidgets [
	| col1 col2 |
	
	"dynamicModelView := self instantiate: OdooUIDynamicModelView"
	
	dynamicModelView := self instantiate: TreePresenter.
	dynamicModelView roots: entity keysSortedSafely.
	
	col1 := self instantiate: TreeColumnPresenter.
	col1 displayBlock: [ :field | field content ].
	
	col2 := self instantiate: TreeColumnPresenter.
	col2 displayBlock: [ :field | (entity at: (field content)) asString ].
	
	dynamicModelView columns: { col1. col2 }.
]

{ #category : #setter }
OdooUIModelFormView >> odooRPCModelBrowser: anOdooRPCModelBrowser [
	odooRPCModelBrowser := anOdooRPCModelBrowser.

]

{ #category : #api }
OdooUIModelFormView >> title [
	^ odooRPCModelBrowser prettyName: entity
]
