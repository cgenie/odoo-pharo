"
Odoo UI.
"
Class {
	#name : #OdooUI,
	#superclass : #ComposablePresenter,
	#instVars : [
		'odooRPC',
		'buttonLogin',
		'modelViewer'
	],
	#category : #Odoo
}

{ #category : #specs }
OdooUI class >> defaultSpec [

	^ SpecLayout composed
		newColumn: [ :col |
			col newRow: [ :row |
				row add: #buttonLogin
			]
		];
	   yourself
]

{ #category : #acccessing }
OdooUI >> buttonLogin [
	^ buttonLogin
]

{ #category : #initialization }
OdooUI >> initializePresenter [ 

	buttonLogin action: [
		OdooUILogin new
			onAccept: [ :l |
				Transcript show: l.
				odooRPC := OdooRPC new.
				odooRPC server: (l at: 1).
				odooRPC db: (l at: 2).
				odooRPC login: (l at: 3) password: (l at: 4)
			];
			openWithSpec
	]
]

{ #category : #initialization }
OdooUI >> initializeWidgets [ 
	buttonLogin := self newButton.
	buttonLogin label: 'Login'
]

{ #category : #api }
OdooUI >> title [
	^ 'Odoo'
]
