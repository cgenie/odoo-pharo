"
A class providing an interface with Odoo REST API.
"
Class {
	#name : #OdooRPC,
	#superclass : #Object,
	#instVars : [
		'db',
		'partnerId',
		'server',
		'uid',
		'zincSession'
	],
	#category : #Odoo
}

{ #category : #jsonrpc }
OdooRPC >> callKw: p [ 
	^ self jsonRpc: '/web/dataset/call_kw' params: p
]

{ #category : #setting }
OdooRPC >> db:  d [
	"comment stating purpose of message"
   db := d
]

{ #category : #'odoo-query' }
OdooRPC >> getSessionInfo [
	^ self jsonRpc: '/web/session/get_session_info'
]

{ #category : #'instance creation' }
OdooRPC >> initialize [
  super initialize.
  zincSession := ZnClient new.
]

{ #category : #jsonrpc }
OdooRPC >> jsonRpc: path [ 
	^ self jsonRpc: path params: Dictionary new
]

{ #category : #jsonrpc }
OdooRPC >> jsonRpc: path params: params [ 
   | d e r error |

   d := self jsonRpcData: params method: 'call'.
     
   e := ZnEntity with: (STONJSON toString: d)
                 type: ZnMimeType applicationJson.

	zincSession
	  http;
	  url: server , path;
	  headerAt: 'Accept' put: 'application/json';
	  headerAt: 'Content-Type' put: 'application/json';
	  entity: e.
	
	r := (STONJSON fromString: zincSession post).
	
	error := r at: #error ifAbsent: nil.
	error ifNil: [ ^ r at: #result ].
	^ Error new signal: error
]

{ #category : #jsonrpc }
OdooRPC >> jsonRpcData: params method: m [
   ^ Dictionary new
       at: #jsonrpc put: '2.0';
       at: #method put: m;
       at: #params put: params;
       yourself.

]

{ #category : #'odoo-auth' }
OdooRPC >> login: l password: p [ 
   | d ret sess |

   d := Dictionary new
     at: #login put: l;
     at: #password put: p;
     at: #db put: db;
     at: #context put: Dictionary new;
     yourself.

	ret := self jsonRpc: '/web/session/authenticate' params: d.

   sess := self getSessionInfo.	
	uid := sess at: #uid.
	partnerId := sess at: #partner_id.
	
	^ ret
]

{ #category : #'odoo-mail' }
OdooRPC >> messageFields [

  ^ { #id . #body . #date . #display_name . #email_from . #model . #needaction . #parent_id . #partner_ids . #res_id . #subject }
]

{ #category : #'odoo-query' }
OdooRPC >> model: model browse: id [ 
   | p |

   p := Dictionary new
     at: #model put: model;
     at: #method put: 'browse';
     at: #args put: { { id } };
     at: #kwargs put: Dictionary new;
     yourself.

	^ self callKw: p
]

{ #category : #'odoo-query' }
OdooRPC >> model: model read: id [
   | p |

   p := Dictionary new
     at: #model put: model;
     at: #method put: 'read';
     at: #args put: { id };
     at: #kwargs put: Dictionary new;
     yourself.

	^ self callKw: p
]

{ #category : #'odoo-mail' }
OdooRPC >> myMessages [
   | d |

   d := { { 'partner_ids' . '=' . partnerId } }.

   ^ self searchRead: 'mail.message' fields: (self messageFields) domain: d
]

{ #category : #'odoo-project' }
OdooRPC >> myTasks [
   | d |

   d := { { 'user_id' . '=' . uid } }.

   ^ self searchRead: 'project.task' domain: d
]

{ #category : #'odoo-project' }
OdooRPC >> myTimesheets [
  | d |

  d := { { 'employee_id.user_id' . '=' . uid } }.

  ^ self searchRead: 'account.analytic.line' fields: (self timesheetFields) domain:d
]

{ #category : #'odoo-query' }
OdooRPC >> searchCount: model domain: aDomain [
	^ self searchCount: model domain: aDomain kwargs: Dictionary new
]

{ #category : #'odoo-query' }
OdooRPC >> searchCount: model domain: aDomain kwargs: k [
	| p |

	p := Dictionary new
		at: #model put: model;
		at: #method put: 'search_count';
		at: #args put: { aDomain };
		at: #kwargs put: k;
	yourself.

	^ self callKw: p
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m [
   ^ self searchRead: m domain: { }
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m domain: domain [ 
   ^ self searchRead: m domain: domain kwargs: Dictionary new
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m domain: domain kwargs: k [
   ^ self searchRead: m fields: { } domain: domain kwargs: k
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m domain: domain limit: l [
	^ self searchRead: m domain: domain limit: l offset: 0
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m domain: domain limit: l offset: o [
	| k |

	k := Dictionary new
		at: #limit put: l;
		at: #offset put: o;
		yourself.

	^ self searchRead: m domain: domain kwargs: k
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m domain: domain limit: l offset: o orderBy: ob [
	| k |

	k := Dictionary new
		at: #limit put: l;
		at: #offset put: o;
		at: #order put: ob;
		yourself.

	^ self searchRead: m domain: domain kwargs: k
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m domain: domain offset: o [
   | k |

   k := Dictionary new
     at: #offset put: o;
     yourself.

   ^ self searchRead: m domain: domain kwargs: k
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m fields: f domain: domain [
   ^ self searchRead: m fields: f domain: domain kwargs: Dictionary new
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m fields: f domain: domain kwargs: k [
   | p |

   p := Dictionary new
     at: #model put: m;
     at: #method put: 'search_read';
     at: #args put: { domain . f };
     at: #kwargs put: k;
     yourself.

	^ self callKw: p
]

{ #category : #'odoo-query' }
OdooRPC >> searchRead: m limit: l [
   ^ self searchRead: m domain: { } limit: l
]

{ #category : #setting }
OdooRPC >> server: s [
  server := s
]

{ #category : #'odoo-project' }
OdooRPC >> taskTimesheets: taskId [
  | d |

  d := { { 'task_id' . '=' . taskId } }.

  ^ self searchRead: 'account.analytic.line' fields: (self timesheetFields) domain:d
]

{ #category : #'odoo-project' }
OdooRPC >> timesheetFields [

  ^ { #id . #date . #project_id . #unit_amount . #user_id . #task_id }
]
