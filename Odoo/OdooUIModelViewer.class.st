"
View a given model as a list.
"
Class {
	#name : #OdooUIModelViewer,
	#superclass : #ComposablePresenter,
	#instVars : [
		'odooRPC',
		'modelName',
		'modelNameLabel',
		'modelListView',
		'toolbar'
	],
	#category : #Odoo
}

{ #category : #specs }
OdooUIModelViewer class >> defaultSpec [
	| rowWidth |
	
	rowWidth := 75.

	^ SpecLayout composed
		newColumn: [ :col |
			col newRow: [ :r |
				r add: #modelNameLabel width: rowWidth.
				r add: #modelName
			] height: 20.
			col newRow: [ :r |
				r add: #modelListView
			] height: 200.
			col newRow: [ :r |
				r add: #toolbar
			]
		]
]

{ #category : #initialization }
OdooUIModelViewer >> initializePresenter [
	modelName acceptBlock: [ :n |
		modelListView model: n
	]
]

{ #category : #initialization }
OdooUIModelViewer >> initializeWidgets [
	modelName := self newTextInput.
	modelName acceptOnCR: true.
	modelNameLabel := self newLabel.
	modelNameLabel label: 'Model name:'.
	
	modelListView := self instantiate: OdooUIModelListView.
	modelListView odooRPC: odooRPC.
	
	toolbar := self instantiate: OkToolbar.
	
	self focusOrder
		add: modelName;
		add: modelListView
]

{ #category : #access }
OdooUIModelViewer >> modelListView [
	^ modelListView
]

{ #category : #accessing }
OdooUIModelViewer >> modelName [
	^ modelName
]

{ #category : #access }
OdooUIModelViewer >> modelNameLabel [
	^ modelNameLabel
]

{ #category : #setter }
OdooUIModelViewer >> odooRPC: anOdooRPC [ 
	odooRPC := anOdooRPC.
	
	modelListView odooRPC: anOdooRPC 
]

{ #category : #api }
OdooUIModelViewer >> title [
	^ 'Odoo model viewer'
]

{ #category : #access }
OdooUIModelViewer >> toolbar [
	^ toolbar
]
