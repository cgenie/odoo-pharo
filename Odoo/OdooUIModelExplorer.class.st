Class {
	#name : #OdooUIModelExplorer,
	#superclass : #ComposablePresenter,
	#instVars : [
		'odooRPC',
		'modelName',
		'modelNameLabel',
		'tabsView',
		'toolbar'
	],
	#category : #Odoo
}

{ #category : #specs }
OdooUIModelExplorer class >> defaultSpec [
	| rowWidth |
	
	rowWidth := 75.

	^ SpecLayout composed
		newColumn: [ :col |
			col newRow: [ :r |
				r add: #modelNameLabel width: rowWidth.
				r add: #modelName
			] height: 20.
			col newRow: [ :r |
				r add: #tabsView
			].
			col newRow: [ :r |
				r add: #toolbar
			] height: 50
		]
]

{ #category : #initialization }
OdooUIModelExplorer >> initializePresenter [
	modelName
		acceptBlock: [ :m | 
			| mlv omb tab |
			omb := OdooRPCModelBrowser with: odooRPC modelName: m.
			mlv := self instantiate: OdooUIModelListView on: omb.
			tab := self newTab.
			tab presenter: mlv.
			tab label: m.
			tabsView addTab: tab.
			mlv
				whenSelectedItemChanged: [ :entity | 
					entity
						ifNotNil: [ (OdooUIModelFormView on: entity odooRPCModelBrowser: omb)
								openWithSpec ] ].
			self needRebuild: false.
			self buildWithSpec ]
]

{ #category : #initialization }
OdooUIModelExplorer >> initializeWidgets [
	modelName := self newTextInput.
	modelName acceptOnCR: true.
	modelName accept: 'product.template'.
	modelNameLabel := self newLabel.
	modelNameLabel label: 'Model name:'.
	
	"modelListView := self newList."
	tabsView := self newTabManager.
	
	toolbar := self instantiate: OkToolbar.
	
	self focusOrder
		add: modelName;
		add: tabsView
]

{ #category : #access }
OdooUIModelExplorer >> modelName [
	^ modelName
]

{ #category : #access }
OdooUIModelExplorer >> modelNameLabel [
	^ modelNameLabel
]

{ #category : #setter }
OdooUIModelExplorer >> odooRPC: anOdooRPC [ 
	odooRPC := anOdooRPC
]

{ #category : #'model access' }
OdooUIModelExplorer >> setModelBeforeInitialization: anOdooRPC [
	odooRPC := anOdooRPC 
]

{ #category : #access }
OdooUIModelExplorer >> tabsView [
	^ tabsView
]

{ #category : #api }
OdooUIModelExplorer >> title [
	^ 'Odoo model explorer'
]

{ #category : #access }
OdooUIModelExplorer >> toolbar [
	^ toolbar
]
