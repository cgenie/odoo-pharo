"
List view for a specific Odoo model.
"
Class {
	#name : #OdooUIModelListView,
	#superclass : #ComposablePresenterWithModel,
	#instVars : [
		'odooRPCPaginated',
		'listView',
		'pageLabel',
		'prevButton',
		'nextButton'
	],
	#category : #Odoo
}

{ #category : #specs }
OdooUIModelListView class >> defaultSpec [
	^ SpecLayout composed
		newColumn: [ :col |
			col newRow: [ :row |
				row add: #listView
			].
			col newRow: [ :row |
				row add: #prevButton.
				row add: #pageLabel.
				row add: #nextButton.
			] height: 20
		]
]

{ #category : #api }
OdooUIModelListView >> fetchItems [
	| items |
	
	items := odooRPCPaginated getPage.
	
	listView items: items
]

{ #category : #initialization }
OdooUIModelListView >> initializePresenter [
	prevButton action: [
		odooRPCPaginated prev
	].
	nextButton action: [
		odooRPCPaginated next
	].
]

{ #category : #initialization }
OdooUIModelListView >> initializeWidgets [
	pageLabel := self newLabel.
	pageLabel label: odooRPCPaginated page asString.

	listView := self instantiate: ListPresenter.
	listView displayBlock: [ :entity |
		odooRPCPaginated odooRPCModelBrowser prettyName: entity
	].

	prevButton := self newButton.
	prevButton label: '<'.
	nextButton := self newButton.
	nextButton label: '>'.

	self fetchItems

]

{ #category : #access }
OdooUIModelListView >> listView [
	^ listView
]

{ #category : #initialization }
OdooUIModelListView >> modelChanged [
	self fetchItems.
	pageLabel label: odooRPCPaginated page asString.
]

{ #category : #access }
OdooUIModelListView >> nextButton [
	^ nextButton
]

{ #category : #setter }
OdooUIModelListView >> odooRPCPaginated: anOdooRPCPaginated [ 
	odooRPCPaginated := anOdooRPCPaginated.
	
]

{ #category : #access }
OdooUIModelListView >> pageLabel [
	^ pageLabel
]

{ #category : #access }
OdooUIModelListView >> prevButton [
	^ prevButton
]

{ #category : #'model access' }
OdooUIModelListView >> setModelBeforeInitialization: anOdooRPCModelBrowser [
	odooRPCPaginated := (OdooRPCPaginated with: anOdooRPCModelBrowser).
	self model: odooRPCPaginated
]

{ #category : #'events-shortcuts' }
OdooUIModelListView >> whenSelectedItemChanged: aBlock [
	listView whenSelectedItemChanged: aBlock
]
