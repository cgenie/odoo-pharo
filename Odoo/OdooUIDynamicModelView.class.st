Class {
	#name : #OdooUIDynamicModelView,
	#superclass : #DynamicComposablePresenter,
	#instVars : [
		'entity'
	],
	#category : #Odoo
}

{ #category : #deprecation }
OdooUIDynamicModelView class >> isDeprecated [ 
	^ true
]

{ #category : #'instance creation' }
OdooUIDynamicModelView class >> on: anEntity [
	| inst |
	inst := self basicNew.
	inst entity: anEntity.
	inst initialize.
	^inst
]

{ #category : #'as yet unclassified' }
OdooUIDynamicModelView >> entityLayout: aDictionary [
	| l |
	l := SpecLayout composed
		newColumn: [ :col |
			aDictionary keysDo: [ :field | | value fieldSel valueSel labelName labelValue widgetName widgetValue |
				value := aDictionary at: field.
				value isCollection ifTrue: [ ^ nil].
				fieldSel := ('accessorTo', field) asSymbol.
				valueSel := ('accessorTo', value asString) asSymbol.
				labelName := self instantiateModels: {fieldSel. #LabelPresenter}.
				widgetName := self perform: fieldSel.
				widgetName label: field.
				labelValue := self instantiateModels: {valueSel. #LabelPresenter}.
				widgetValue := self perform: valueSel.
				widgetValue label: value asString.
				col newRow: [ :row |
					row add: fieldSel width: 55.
					row add: valueSel
				]
			].
		].
	
	^ l
]
