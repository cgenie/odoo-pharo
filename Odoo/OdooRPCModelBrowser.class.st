"
I am an OdooRPC proxy with fixed model and domain.
"
Class {
	#name : #OdooRPCModelBrowser,
	#superclass : #Object,
	#instVars : [
		'odooRPC',
		'modelName',
		'domain'
	],
	#category : #Odoo
}

{ #category : #'as yet unclassified' }
OdooRPCModelBrowser class >> with: anOdooRPC modelName: mString [
	^ self with: anOdooRPC
		modelName: mString
		domain: { }
]

{ #category : #'as yet unclassified' }
OdooRPCModelBrowser class >> with: anOdooRPC modelName: mString domain: aDomain [
	^ self new
		odooRPC: anOdooRPC;
		modelName: mString;
		domain: aDomain
]

{ #category : #setting }
OdooRPCModelBrowser >> domain: aCollection [ 
	domain := aCollection
]

{ #category : #api }
OdooRPCModelBrowser >> limit: anInteger [ 
	^ self limit: anInteger offset: 0
]

{ #category : #api }
OdooRPCModelBrowser >> limit: lInteger offset: oInteger [
	^ odooRPC searchRead: modelName domain: domain limit: lInteger offset: oInteger
]

{ #category : #api }
OdooRPCModelBrowser >> limit: lInteger offset: oInteger orderBy: obString [
	^ odooRPC searchRead: modelName domain: domain limit: lInteger offset: oInteger orderBy: obString
]

{ #category : #setting }
OdooRPCModelBrowser >> modelName: aString [ 
	modelName := aString
]

{ #category : #setting }
OdooRPCModelBrowser >> odooRPC: anOdooRPC [ 
	odooRPC := anOdooRPC
]

{ #category : #api }
OdooRPCModelBrowser >> offset: oInteger [
	^ odooRPC searchRead: modelName domain: domain offset: oInteger
]

{ #category : #'as yet unclassified' }
OdooRPCModelBrowser >> prettyName: entity [
	^ '[', ((entity at: #id) asString), '] ', (entity at: #display_name)
]

{ #category : #api }
OdooRPCModelBrowser >> searchCount [
	^ odooRPC searchCount: modelName domain: domain
]
