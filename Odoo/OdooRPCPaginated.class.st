Class {
	#name : #OdooRPCPaginated,
	#superclass : #Model,
	#instVars : [
		'odooRPCModelBrowser',
		'page',
		'pageSize'
	],
	#category : #Odoo
}

{ #category : #'instance creation' }
OdooRPCPaginated class >> with: anOdooRPCModelBrowser [
	^ self with: anOdooRPCModelBrowser page: 1 pageSize: 10
]

{ #category : #'instance creation' }
OdooRPCPaginated class >> with: anOdooRPC modelName: aString domain: anArray [
	^ self with: (OdooRPCModelBrowser with: anOdooRPC modelName: aString domain: anArray)
		page: 1
		pageSize: 10
	
]

{ #category : #'instance creation' }
OdooRPCPaginated class >> with: anOdooRPC modelName: mString domain: aDomain page: pInt pageSize: psInt [
	^ self with: (OdooRPCModelBrowser with: anOdooRPC modelName: mString domain: aDomain);
		page: pInt;
		pageSize: psInt.	
]

{ #category : #'instance creation' }
OdooRPCPaginated class >> with: anOdooRPC modelName: mString domain: aDomain pageSize: psInt [
	^ self with: (OdooRPCModelBrowser with: anOdooRPC modelName: mString domain: aDomain)
		page: 1
		pageSize: psInt
	
]

{ #category : #'instance creation' }
OdooRPCPaginated class >> with: anOdooRPCModelBrowser page: pInt pageSize: psInt [
	^ self new
		odooRPCModelBrowser: anOdooRPCModelBrowser;
		page: pInt;
		pageSize: psInt
]

{ #category : #'instance creation' }
OdooRPCPaginated class >> with: anOdooRPCModelBrowser pageSize: psInt [
	^ self with: anOdooRPCModelBrowser page: 1 pageSize: psInt
]

{ #category : #api }
OdooRPCPaginated >> getPage [
	^ odooRPCModelBrowser limit: pageSize offset: (self offset) orderBy: 'id ASC'
]

{ #category : #api }
OdooRPCPaginated >> limit [
	^ odooRPCModelBrowser searchCount
]

{ #category : #setting }
OdooRPCPaginated >> next [
	self page: page + 1
]

{ #category : #setting }
OdooRPCPaginated >> odooRPCModelBrowser [
	^ odooRPCModelBrowser
]

{ #category : #setting }
OdooRPCPaginated >> odooRPCModelBrowser: anOdooRPCModelBrowser [
	odooRPCModelBrowser := anOdooRPCModelBrowser
]

{ #category : #access }
OdooRPCPaginated >> offset [
	^ (page - 1) * pageSize
]

{ #category : #access }
OdooRPCPaginated >> page [
	^ page
]

{ #category : #setting }
OdooRPCPaginated >> page: anInt [
	page := anInt.
	self valueChanged
]

{ #category : #setting }
OdooRPCPaginated >> pageSize: anInt [
	pageSize := anInt.
	self valueChanged
]

{ #category : #setting }
OdooRPCPaginated >> prev [
	self page: ((page - 1) max: 1)
]
